package it.unibo.oop.lab.enum1;

import it.unibo.oop.lab.socialnetwork.User;
import it.unibo.oop.lab.enum1.SportSocialNetworkUserImpl;

/**
 * This is going to act as a test for
 * {@link it.unibo.oop.lab.enum1.SportSocialNetworkUserImpl}
 * 
 * 1) Realize the same test as the previous exercise, this time using
 * enumeration Sport
 * 
 * 2) Run it: every test must return true.
 * 
 */
public final class TestSportByEnumeration {

    private TestSportByEnumeration() {
    }

    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
        /*
         * [TEST DEFINITION]
         * 
         * By now, you know how to do it
         */
        // TODO
    	
    	final SportSocialNetworkUserImpl<User> steve = new SportSocialNetworkUserImpl<>("Steve", "Inno", "sinno");
    	final SportSocialNetworkUserImpl<User> mike = new SportSocialNetworkUserImpl<>("Mike", "Inno", "minno");
    	
    	steve.addSport(Sport.BIKE);
    	steve.addSport(Sport.F1);
    	
    	mike.addSport(Sport.SOCCER);
    	
    	System.out.println("Steve likes bike: " + steve.hasSport(Sport.BIKE));
    	System.out.println("Steve doesn't like volley: " + !steve.hasSport(Sport.VOLLEY));
    	System.out.println("Mike doesn't bike: " + !mike.hasSport(Sport.BIKE));
    	
    }

}
